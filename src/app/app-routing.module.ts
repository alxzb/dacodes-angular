import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CatsComponent } from './components/cats/cats.component';
import { FavouriteComponent } from './components/favourite/favourite.component';

const routes: Routes = [
	{ path: 'home', component: CatsComponent },
	{ path: 'favorites', component: FavouriteComponent },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
