import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Cat } from '../components/cats/cat';

@Injectable({
	providedIn: 'root'
})
export class CatapiService {

	private apiUrl = `https://api.thecatapi.com/v1/`;
	private httpOptions = {
		headers: new HttpHeaders({
			'x-api-key': '0deb10af-017e-4cb1-b4a8-c3d08d691587',
			'Content-Type': 'application/json'
		})
	}

	constructor(private http: HttpClient) { }

	getCats(): Observable<Cat[]> {
		return this.http.get<Cat[]>(`${this.apiUrl}images/search?limit=12`, this.httpOptions);
	}

	getFavourites(): Observable<Cat[]> {
		return this.http.get<Cat[]>(`${this.apiUrl}favourites?limit=12`, this.httpOptions).pipe(map(data => data));
	}

	setFavourite(id: string): Observable<any> {
		return this.http.post<Cat>(`${this.apiUrl}favourites`, { image_id: id }, this.httpOptions).pipe(map(data => data));
	}
	deleteFavourite(id: string): Observable<any> {
		return this.http.delete<Cat>(`${this.apiUrl}favourites/${id}`, this.httpOptions).pipe(map(data => data));
	}
}
