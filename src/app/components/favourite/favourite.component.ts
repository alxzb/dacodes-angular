import { Component, OnInit } from '@angular/core';

import { Cat } from './../cats/cat';
import { CatapiService } from '../../services/catapi.service';

@Component({
	selector: 'app-favourite',
	templateUrl: './favourite.component.html',
	styleUrls: ['./favourite.component.css']
})
export class FavouriteComponent implements OnInit {
	cats: Cat[] = [];

	constructor(private catapiService: CatapiService) { }

	ngOnInit(): void {
		this.getFavourites();
	}

	getFavourites(): void {
		this.catapiService.getFavourites().subscribe(cats => this.cats = cats);
	}

	deleteFavourite(id: string): void {
		this.catapiService.deleteFavourite(id).subscribe(response => console.log(response));
	}
}
