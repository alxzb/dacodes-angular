import { Component, OnInit } from '@angular/core';

import { Cat } from './cat';
import { CatapiService } from '../../services/catapi.service';

@Component({
	selector: 'app-cats',
	templateUrl: './cats.component.html',
	styleUrls: ['./cats.component.css']
})
export class CatsComponent implements OnInit {

	/* cats: Cat[] = [
		{ id: 1, img: 'https://cdn2.thecatapi.com/images/blh.jpg', fav: false },
		{ id: 2, img: 'https://cdn2.thecatapi.com/images/c21.jpg', fav: false },
		{ id: 3, img: 'https://cdn2.thecatapi.com/images/cj4.jpg', fav: false },
		{ id: 4, img: 'https://cdn2.thecatapi.com/images/ck9.jpg', fav: false },
	]; */
	cats: Cat[] = [];
	// selectedCat: Cat;

	constructor(private catapiService: CatapiService) { }

	ngOnInit(): void {
		this.getCats();
	}

	onSelectedCat(cat: Cat): void {
		// this.selectedCat = cat;
	}

	getCats(): void {
		this.catapiService.getCats().subscribe(cats => this.cats = cats);
	}

	setFavourite(id: string) {
		this.catapiService.setFavourite(id).subscribe(response => console.log(response));
	}

}
