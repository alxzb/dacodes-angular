export interface Cat {
	id: Number;
	url?: String;
	image?: Object;
	image_id?: String;
}
