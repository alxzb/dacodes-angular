import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

/** Components */
import { AppComponent } from './app.component';
import { CatsComponent } from './components/cats/cats.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';

/** Services */
import { CatapiService } from './services/catapi.service';
import { FavouriteComponent } from './components/favourite/favourite.component';

@NgModule({
	declarations: [
		AppComponent,
		CatsComponent,
		NavbarComponent,
		FavouriteComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
	],
	providers: [
		CatapiService,
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
